package com.memorize.memorize.rest;

import com.memorize.memorize.dto.MemorizeFilter;
import com.memorize.memorize.dto.SummaryTotalProgress;
import com.memorize.memorize.model.entity.Memorize;
import com.memorize.memorize.service.MemorizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


//@CrossOrigin(origins= {"http://localhost:4200"})
@RestController
@RequestMapping("/memorize")
public class MemorizeController {

    @Autowired
    private MemorizeService memorizeService;


    @GetMapping("/getListMemorize/{id}")
    public List<Memorize> getListMemorize(@PathVariable Integer id){
        return memorizeService.findListByIdType(id);
    }


    @PostMapping("/findLisByFilter")
    public List<Memorize> findLisByFilter(@RequestBody MemorizeFilter filter){
        return memorizeService.findLisByFilter(filter);
    }

    @PostMapping("/saveMemorize")
    public boolean saveMemorize(@RequestBody Memorize memorize){
        memorizeService.memorizeSave(memorize);
        return true;
    }

    @PostMapping("/likeFirst")
    public Memorize likeFirst(@RequestBody Memorize memorize){
        List<Memorize> list = memorizeService.like("%"+memorize.getEnglish()+"%", "%"+memorize.getSpanish()+"%");
        return !list.isEmpty() ? list.get(0) : new Memorize();
    }

    @PostMapping("/updateMemorizesStatus")
    public boolean updateMemorizesStatus(@RequestBody List<Memorize> memorizes){
        memorizeService.updateMemorizesStatus(memorizes);
        return true;
    }


    @GetMapping("/retrieveSummaryTotalProgress")
    public SummaryTotalProgress retrieveSummaryTotalProgress(){
        return memorizeService.retrieveSummaryTotalProgress();
    }


//    @GetMapping("/test")
//    public void test(){
//        memorizeService.test();
//    }
}
