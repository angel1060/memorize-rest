package com.memorize.memorize.service;

import com.memorize.memorize.dto.MemorizeFilter;
import com.memorize.memorize.dto.SummaryTotalProgress;
import com.memorize.memorize.model.entity.Memorize;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MemorizeService {
    List<Memorize> findListByIdType(Integer typeId);
    List<Memorize> findLisByFilter(MemorizeFilter filter);
    void memorizeSave(Memorize memorize);
    List<Memorize> like(String english, String spanish);
    void updateMemorizesStatus(List<Memorize> memorizes);
    SummaryTotalProgress retrieveSummaryTotalProgress();
//    void test();
}
