package com.memorize.memorize.service.impl;

import com.memorize.memorize.dto.MemorizeFilter;
import com.memorize.memorize.dto.SummaryTotalProgress;
import com.memorize.memorize.model.entity.Memorize;
import com.memorize.memorize.model.repository.IMemorizeRepository;
import com.memorize.memorize.model.repository.MemorizeRepository;
import com.memorize.memorize.service.MemorizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemorizeServiceImpl implements MemorizeService {
    @Autowired
    private IMemorizeRepository iMemorizeRepository;

    @Autowired
    private MemorizeRepository memorizeRepository;

    @Override
    public List<Memorize> findListByIdType(Integer typeId){
        return iMemorizeRepository.findListByIdType(typeId);
    }

    @Override
    public void memorizeSave(Memorize memorize){
        if(memorize.getId() == null){
            memorize.setTypeId(1);
            memorize.setScor(0);
            memorize.setFaults(0);
            memorize.setHelps(0);
            memorize.setAdvancePercentage(0);
            memorize.setRequiredRepetitions(20);
        }
        iMemorizeRepository.save(memorize);
    }

    @Override
    public List<Memorize> like(String english, String spanish){
        return iMemorizeRepository.like(english, spanish);
    }

    @Override
    public void updateMemorizesStatus(List<Memorize> memorizes){
        for (Memorize m: memorizes) {
            // iMemorizeRepository.save(m);
            iMemorizeRepository.updateStatus(
                    m.getRequiredRepetitions(),
                    m.getScor(),
                    m.getFaults(),
                    m.getHelps(),
                    m.getAdvancePercentage(),
                    m.getId()
            );
        }
    }

    public List<Memorize> findLisByFilter(MemorizeFilter filter){
        return memorizeRepository.findLisByFilter(filter);
    }

    public SummaryTotalProgress retrieveSummaryTotalProgress(){
        return memorizeRepository.retrieveSummaryTotalProgress();
    }

//    public void test(){
//        System.out.println("Obje: "+memorizeRepository.findBySpanish("e"));
//
//    }
}
