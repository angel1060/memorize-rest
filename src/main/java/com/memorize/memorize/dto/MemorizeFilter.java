package com.memorize.memorize.dto;

public class MemorizeFilter {
    private Integer typeWord;
    private boolean minor100;
    private boolean sortMinor100;
    private Integer limit;

    public Integer getTypeWord() {
        return typeWord;
    }

    public void setTypeWord(Integer typeWord) {
        this.typeWord = typeWord;
    }

    public boolean isMinor100() {
        return minor100;
    }

    public void setMinor100(boolean minor100) {
        this.minor100 = minor100;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public boolean isSortMinor100() {
        return sortMinor100;
    }

    public void setSortMinor100(boolean sortMinor100) {
        this.sortMinor100 = sortMinor100;
    }
}
