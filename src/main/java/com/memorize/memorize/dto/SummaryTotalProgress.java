package com.memorize.memorize.dto;

public class SummaryTotalProgress {
    private int remainingMomorize;
    private Double percentRemainingMomorize;

    public int getRemainingMomorize() {
        return remainingMomorize;
    }

    public void setRemainingMomorize(int remainingMomorize) {
        this.remainingMomorize = remainingMomorize;
    }

    public Double getPercentRemainingMomorize() {
        return percentRemainingMomorize;
    }

    public void setPercentRemainingMomorize(Double percentRemainingMomorize) {
        this.percentRemainingMomorize = percentRemainingMomorize;
    }
}
