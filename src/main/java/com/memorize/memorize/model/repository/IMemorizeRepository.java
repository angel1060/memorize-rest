package com.memorize.memorize.model.repository;

import com.memorize.memorize.dto.SummaryTotalProgress;
import com.memorize.memorize.model.entity.Memorize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IMemorizeRepository extends JpaRepository<Memorize, Integer> {

    @Query(value="select m from Memorize m where m.typeId = :typeId")
    List<Memorize> findListByIdType(@Param("typeId") Integer typeId);

    // typeWords, minor100, limit
    @Query(value="select m from Memorize m where m.typeId = :typeWords and m.advancePercentage < 100")
    List<Memorize> findListByIdTypeAndAdvancePercentageMinor100(@Param("typeWords") Integer typeWords);

    /*
    @Query(value="select m from Memorize m where m.typeId = ?1 limit 1")
    List<Memorize> findListByIdTypeAndAdvancePercentageInclude100(Integer typeWords);
    */

    @Query(value="select m from Memorize m where m.english like :english or m.spanish like :spanish")
    List<Memorize> like(@Param("english") String english, @Param("spanish") String spanish);

    // @Query("SELECT new de.smarterco.example.dto.UserNameDTO(u.id, u.name) FROM User u WHERE u.name = :name")
//    @Query("SELECT new com.memorize.memorize.dto.SummaryTotalProgress(u.id, u.name) FROM User u WHERE u.name = :name")
//    SummaryTotalProgress retrieveSummaryTotalProgress();

    @Transactional
    @Modifying
    @Query(value="update Memorize m set m.requiredRepetitions = :requiredRepetitions, m.scor = :scor, m.faults = :faults, m.helps = :helps, m.advancePercentage = :advancePercentage where m.id = :id")
    void updateStatus(@Param("requiredRepetitions") Integer requiredRepetitions,
                          @Param("scor") Integer scor,
                          @Param("faults") Integer faults,
                          @Param("helps") Integer helps,
                          @Param("advancePercentage") Integer advancePercentage,
                          @Param("id") Integer id);

    /*

    @Column private Integer requiredRepetitions;
    @Column private Integer scor;
    @Column private Integer faults;
    @Column private Integer helps;
    @Column private Integer advancePercentage;
    * */

//    @Query("testOrmXml")
}
