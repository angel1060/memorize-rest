package com.memorize.memorize.model.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.memorize.memorize.dto.MemorizeFilter;
import com.memorize.memorize.dto.SummaryTotalProgress;
import com.memorize.memorize.model.entity.Memorize;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class MemorizeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    // typeWords, minor100, limit
    //@Query(value="select m from Memorize m where m.typeId = :typeWords and m.advancePercentage < 100")
//    public List<Memorize> findLisByFilter(MemorizeFilter filter) {
//        String minor100 = "";
//        String rand = "";
//        String strQuery = "select m from Memorize m where m.typeId = :typeWord ORDER BY RAND()";
//
//        if(filter.getLimit() == null){
//            filter.setLimit(50);
//        }
//
//        if(filter.getTypeWord() == null){
//            filter.setTypeWord(1);
//        }
//
//        if(filter.isMinor100()){
//            strQuery = "select m from Memorize m where m.typeId = :typeWord and m.advancePercentage < 100 ORDER BY m.advancePercentage asc";
//        }
//
//        if(filter.isRand()){
//            strQuery = "select m from Memorize m where m.typeId = :typeWord and m.advancePercentage < 100 ORDER BY RAND()";
//        }
//
//        TypedQuery<Memorize> query = entityManager.createQuery(strQuery, Memorize.class);
//        query.setParameter("typeWord", filter.getTypeWord());
//        query.setMaxResults(filter.getLimit()).getResultList();
//
//        return query.getResultList();
//    }


        public List<Memorize> findLisByFilter(MemorizeFilter filter) {
            String minor100 = "";
            String sortMinor100 = "";

            if(filter.getLimit() == null){
                filter.setLimit(50);
            }

            if(filter.getTypeWord() == null){
                filter.setTypeWord(1);
            }

            if(filter.isMinor100()){
                minor100 = " and m.advancePercentage < 100 ";
            }

            if(filter.isSortMinor100()){
                sortMinor100 = " m.advancePercentage asc, ";
            }

            String strQuery = "select m from Memorize m where m.typeId = :typeWord "+ minor100 +" ORDER BY "+ sortMinor100 + " RAND() ";

            TypedQuery<Memorize> query = entityManager.createQuery(strQuery, Memorize.class);
            query.setParameter("typeWord", filter.getTypeWord());
            query.setMaxResults(filter.getLimit()).getResultList();

            return query.getResultList();
        }

        public SummaryTotalProgress retrieveSummaryTotalProgress(){
            SummaryTotalProgress summary = new SummaryTotalProgress();
            String sql = "select count(advancePercentage) as remainingMomorize, sum(advancePercentage)/ count(advancePercentage) as percentRemainingMomorize  from Memorize where advancePercentage < 100";
//
//            TypedQuery<SummaryTotalProgress> queryx = entityManager.createQuery(sql, SummaryTotalProgress.class);

            Query query = entityManager.createNativeQuery(sql);
            //System.out.println((SummaryTotalProgress)query.getSingleResult());
            List<Object[]> list = query.getResultList();
            summary.setRemainingMomorize(Integer.parseInt(list.get(0)[0].toString()));
            summary.setPercentRemainingMomorize(Double.parseDouble(list.get(0)[1].toString()));

//            for(Object[] q1 : list){
//
//                String name = q1[0].toString();
//                System.out.println(name);
//                //..
//                //do something more on
//            }

//            return queryx.getSingleResult();

            return summary;
        }


//        public List<Memorize> findBySpanish(String spanish){
//            TypedQuery query = entityManager.createNamedQuery("Memorize.findX", Memorize.class);
//
//            return query.getResultList();
//        }


}
