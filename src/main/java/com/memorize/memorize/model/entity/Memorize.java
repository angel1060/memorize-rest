package com.memorize.memorize.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Memorize {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column private Integer id;
    @Column private String spanish;
    @Column private String english;
    @Column private String example;
    @Column private Integer typeId;
    @Column private Integer requiredRepetitions;
    @Column private Integer scor;
    @Column private Integer faults;
    @Column private Integer helps;
    @Column private String imageUrl;
    @Column private Integer advancePercentage;
    @Column private String resourcelink;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy="memorize")
    private List<Alternative> alternatives;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSpanish() {
        return spanish;
    }

    public void setSpanish(String spanish) {
        this.spanish = spanish;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getScor() {
        return scor;
    }

    public void setScor(Integer scor) {
        this.scor = scor;
    }

    public Integer getFaults() {
        return faults;
    }

    public void setFaults(Integer faults) {
        this.faults = faults;
    }

    public List<Alternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<Alternative> alternatives) {
        this.alternatives = alternatives;
    }

    public Integer getRequiredRepetitions() {
        return requiredRepetitions;
    }

    public void setRequiredRepetitions(Integer requiredRepetitions) {
        this.requiredRepetitions = requiredRepetitions;
    }

    public Integer getHelps() {
        return helps;
    }

    public void setHelps(Integer helps) {
        this.helps = helps;
    }

    public Integer getAdvancePercentage() {
        return advancePercentage;
    }

    public void setAdvancePercentage(Integer advancePercentage) {
        this.advancePercentage = advancePercentage;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getResourcelink() {
        return resourcelink;
    }

    public void setResourcelink(String resourcelink) {
        this.resourcelink = resourcelink;
    }
}
