package com.memorize.memorize.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Alternative {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column private Integer alternativeId;
    @Column private String english;

    @JsonBackReference
    @OneToOne
    @JoinColumn(name="memorizeId", referencedColumnName="id")
    private Memorize memorize;

    public Integer getAlternativeId() {
        return alternativeId;
    }

    public void setAlternativeId(Integer alternativeId) {
        this.alternativeId = alternativeId;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public Memorize getMemorize() {
        return memorize;
    }

    public void setMemorize(Memorize memorize) {
        this.memorize = memorize;
    }
}
